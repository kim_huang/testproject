//
//  PTAppDelegate.h
//  Tetst
//
//  Created by Kim on 14-1-13.
//  Copyright (c) 2014年 FabriQate Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
